# Transfer Framework
This directory includes a simple framework for studying the data transfer between the host and device.

Host code is available in both native XRT API and OpenCL API. Codes are named accordingly. The default host API for current EFT FPGA study is OpenCL so it's what used in the makefile

The cluster directory includes dummy cluster classes.

## Build instructions 
As a first step, please always make sure that the device and Vitis environment are ready: 
-  The shell is propoerly programmed for two-staged cards (U250) after each system reboot. Only need to be done once for each reboot, but this requires system administrator to perform.
-  Vitis environment and platform file setup
```bash
source /tools/Xilinx/Vitis/2022.1/settings64.sh
source /opt/xilinx/xrt/setup.sh 
export PLATFORM_REPO_PATHS=/opt/xilinx/platforms/xilinx_u250_gen3x16_xdma_4_1_202210_1/xilinx_u250_gen3x16_xdma_4_1_202210_1.xpfm
```

To make and run sw_emu
```bash
make run_sw_emu
```

To make and run hw_emu
```bash
make run_hw_emu
```

To make and run hw target
```bash
make run_hw
```

This command will automatically compile, setup variables and run. If you need to do a specific step, append the step you want. For example
```bash
# See Makefile for more details
# Make host
make host_<sw_emu or hw_emu or hw>

# Create emconfig file
make emconfig_<sw_emu or hw_emu or hw>

# Make kernel xo file
make xo_<sw_emu or hw_emu or hw>

# Make kernel xclbin file
make xclbin_<sw_emu or hw_emu or hw>
```
Building `hw` takes a very long time (~90min). Therefore, kernels (all three targes) are already built for each tag in this repo. You can find them on eepp-ml2 server @ `/home/zhcui/Share`

## Optional cmds (not tested)
```bash
make -jN # -jN is optional for multi-threads
```