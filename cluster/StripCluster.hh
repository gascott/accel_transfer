#ifndef STRIP_CLUSTER_HH
#define STRIP_CLUSTER_HH

#include "UncalibratedMeasurement.hh"
#include <array>

class StripCluster : public UncalibratedMeasurement
{
public:
    // Constructors and destructor
    StripCluster() = default;
    StripCluster(std::array<float, 3> globalPosition,
                 int channelsInPhi,
                 int hitsInThirdTimeBin,
                 std::array<float, 3> localPosition,
                 std::array<float, 9> localCovariance,
                 unsigned int identifierHash);
    virtual ~StripCluster() = default;

    std::array<float, 3> m_globalPosition;
    int m_channelsInPhi;
    int m_hitsInThirdTimeBin;
    // rdoList

    // Getter
    std::array<float, 3> getGlobalPosition();
    int getChannelsInPhi();
    int getHitsInThirdTimeBin();

    // Setter
    void setGlobalPosition(std::array<float, 3> globalPosition);
    void setChannelsInPhi(int channelsInPhi);
    void setHitsInThirdTimeBin(int hitsInThirdTimeBin);
};

#endif