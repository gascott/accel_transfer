#include "UncalibratedMeasurement.hh"
#include <array>

UncalibratedMeasurement::UncalibratedMeasurement(std::array<float, 3> localPosition, std::array<float, 9> localCovariance, unsigned int identifierHash)
    : m_localPosition(localPosition),
      m_localCovariance(localCovariance),
      m_identifierHash(identifierHash)
{
}

std::array<float, 3> UncalibratedMeasurement::getLocalPosition()
{
    return m_localPosition;
}

std::array<float, 9> UncalibratedMeasurement::getLocalCovariance()
{
    return m_localCovariance;
}

unsigned int UncalibratedMeasurement::getIdentifierHash()
{
    return m_identifierHash;
}

void UncalibratedMeasurement::setLocalPosition(std::array<float, 3> localPosition)
{
    m_localPosition = localPosition;
}

void UncalibratedMeasurement::setLocalCovariance(std::array<float, 9> localCovariance)
{
    m_localCovariance = localCovariance;
}

void UncalibratedMeasurement::setIdentifierHash(unsigned int identifierHash)
{
    m_identifierHash = identifierHash;
}