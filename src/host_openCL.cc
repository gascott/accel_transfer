// A host programe initiates transfer between the host and device
// Modified from Vitis example
// zhaoyuan.cui@cern.ch
// Physics department, University of Arizona

#define CL_HPP_CL_1_2_DEFAULT_BUILD
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY 1
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define DATA_SIZE 5

// #include "include/cluster.hh"
#include <iostream>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <CL/cl2.hpp>

// Forward declaration of utility functions included at the end of this file
std::vector<cl::Device> get_xilinx_devices();
char *read_binary_file(const std::string &xclbin_file_name, unsigned &nb);

int main(int argc, char **argv)
{
    // PixelCluster cluster(1,2);
    // std::cout<<"Channesl in phi is: "<<cluster.getChannelsInPhi()<<std::endl;
    // std::cout<<"Channesl in eta is: "<<cluster.getChannelsInEta()<<std::endl;

    // Initialize the OpenCL environment
    cl_int err;
    std::string binaryFile = (argc != 2) ? "transfer.xclbin" : argv[1];
    unsigned fileBufSize;
    std::vector<cl::Device> devices = get_xilinx_devices();
    devices.resize(1);
    cl::Device device = devices[0];
    cl::Context context(device, NULL, NULL, NULL, &err);
    char *fileBuf = read_binary_file(binaryFile, fileBufSize);
    cl::Program::Binaries bins{{fileBuf, fileBufSize}};
    cl::Program program(context, devices, bins, NULL, &err);
    cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    cl::Kernel krnl_transfer(program, "transfer", &err);

    // Create buffers and initialize
    // Create the buffers and allocate memory
    cl::Buffer in_buff(context, CL_MEM_READ_ONLY, sizeof(float) * DATA_SIZE, NULL, &err);
    cl::Buffer out_buff(context, CL_MEM_READ_WRITE, sizeof(float) * DATA_SIZE, NULL, &err);

    // Map host-side buffer memory to user-space pointers
    float *input = (float *)q.enqueueMapBuffer(in_buff, CL_TRUE, CL_MAP_WRITE, 0, sizeof(float) * DATA_SIZE);
    float *output = (float *)q.enqueueMapBuffer(out_buff, CL_TRUE, CL_MAP_WRITE | CL_MAP_READ, 0, sizeof(float) * DATA_SIZE);

    // Initialize the input buffers
    for (int i = 0; i < DATA_SIZE; i++)
    {
        input[i] = rand() % DATA_SIZE;
        output[i] = 0;
    }

    // Map buffers to kernel arguments
    krnl_transfer.setArg(0, in_buff);
    krnl_transfer.setArg(1, out_buff);
    krnl_transfer.setArg(2, DATA_SIZE);

    // Schedule transfer of inputs to device memory,
    // execution of kernel, and transfer of outputs back to host memory
    q.enqueueMigrateMemObjects({in_buff}, 0); // 0 means from host
    q.enqueueTask(krnl_transfer);
    q.enqueueMigrateMemObjects({out_buff}, CL_MIGRATE_MEM_OBJECT_HOST);

    q.finish();

    // Check output
    bool match = true;

    for (int i = 0; i < DATA_SIZE; i++)
    {
        // Print input and output anyway
        std::cout << "input is: " << input[i] << " output is: " << output[i] << std::endl;
        if (output[i] != input[i])
        {
            std::cout << "Error: Results mismatch" << std::endl;
            std::cout << "i = " << i << " result = " << input[i] << std::endl;
            match = false;
            break;
        }
    }

    delete[] fileBuf;
    std::cout << "TEST " << (match ? "Passed" : "Failed") << std::endl;

    return (match ? EXIT_SUCCESS : EXIT_FAILURE);
}

std::vector<cl::Device> get_xilinx_devices()
{
    size_t i;
    cl_int err;
    std::vector<cl::Platform> platforms;
    err = cl::Platform::get(&platforms);
    cl::Platform platform;
    for (i = 0; i < platforms.size(); i++)
    {
        platform = platforms[i];
        std::string platformName = platform.getInfo<CL_PLATFORM_NAME>(&err);
        if (platformName == "Xilinx")
        {
            std::cout << "INFO: Found Xilinx Platform" << std::endl;
            break;
        }
    }
    if (i == platforms.size())
    {
        std::cout << "ERROR: Failed to find Xilinx platform" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Getting ACCELERATOR Devices and selecting 1st such device
    std::vector<cl::Device> devices;
    err = platform.getDevices(CL_DEVICE_TYPE_ACCELERATOR, &devices);
    return devices;
}

char *read_binary_file(const std::string &xclbin_file_name, unsigned &nb)
{
    if (access(xclbin_file_name.c_str(), R_OK) != 0)
    {
        printf("ERROR: %s xclbin not available please build\n", xclbin_file_name.c_str());
        exit(EXIT_FAILURE);
    }
    // Loading XCL Bin into char buffer
    std::cout << "INFO: Loading '" << xclbin_file_name << "'\n";
    std::ifstream bin_file(xclbin_file_name.c_str(), std::ifstream::binary);
    bin_file.seekg(0, bin_file.end);
    nb = bin_file.tellg();
    bin_file.seekg(0, bin_file.beg);
    char *buf = new char[nb];
    bin_file.read(buf, nb);
    return buf;
}